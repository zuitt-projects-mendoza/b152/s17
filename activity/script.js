const students = ['Alex','Bob','Charles','Dave']

function addStudent(name) {
	students.push(name)
	console.log(name + ' was added to the student list')
	console.log(students)

}

function countStudents() {
	console.log('The number of students is ',students.length)

}

function printStudents() {

	students.sort()
	students.forEach(function(name) {
		console.log('Name of Student: ',name)
	})

}

addStudent('Carlo')
addStudent('Adrian')
countStudents()
printStudents()