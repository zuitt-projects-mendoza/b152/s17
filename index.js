// storing multiple values using variables

let student1 = '2020-01-17'
let student2 = '2020-01-20'
let student3 = '2020-01-25'
let student4 = '2020-02-25'

console.log(student1)
console.log(student2)
console.log(student3)

// storing multiple values in an array

const studentNumbers = ['2020-01-17','2020-01-20','2020-01-25','2020-02-25']
console.log(studentNumbers)

// outputs the value of the element in the index 1 of the studengNumbers array
// all arrays starts with index 0
// syntax: arrayName[index of the value we want to output]
console.log(studentNumbers[1])
console.log('Index 3 of studentNumbers: ',studentNumbers[3])
console.log(studentNumbers[4]) // returns undefined since there is no value defined for the fourth index of the studentNumbers array

// arrays are declared using the square brackets aka array literals
// each data stored inside an array is called an array element

const emptyArray = [] // declares an empty array
const grades = [75,85.5,92,94] // an array of numbers
const computerBrands = ['Acer','Asus','Lenovo','Apple','Redfox','Gateway']

console.log('Empyty Array Sample: ', emptyArray)
console.log(grades)
console.log('Computer Brand Array: ',computerBrands)

// all elements inside an array should have the same data type
// all elements inside an array should be related with each other

const mixedArr = [12,'Asus',undefined,null,{}] // not ideal

const fruits = ['Apple','Orange','Kiwi,','Dragon Fruit']
console.log('Value of fruits array before push()',fruits)

// push() function adds an element at the end of an array
fruits.push('Mango')
console.log('New value of fruits array after push()',fruits)

console.log('Value of fruits before pop()',fruits)

// removes the last element in the array
fruits.pop()
console.log('Value of fruits after pop()',fruits)

//unshift adds one or more elements at the beginning of the array
console.log('Value of fruits before unshift()',fruits)
fruits.unshift('Strawberry') //adds strawberry to the beginning of the fruits array
console.log(fruits)
fruits.unshift('Banana','Raisins')
console.log(fruits)

// shift() removes the first element in our array

let removedFruit = fruits.shift() // expected output is banana is removed from the fruits array
console.log('You succesfully removed the fruit: ',removedFruit)
console.log(fruits)

// reverse reverses the order of elements in an array
fruits.reverse()
console.log('Value of fruits after reverse()',fruits)

const tasks = [
	'drink html',
	'eat JS',
	'inhale CSS',
	'bale sass'
]

console.log('Value of tasks before sort()',tasks)
tasks.sort()
console.log('Value of tasks after sort()',tasks)

const oddNumbers = [3,5,1,9,8]

// arranges the elements in an alphanumeric sequence
console.log('Value of oddNumbers before sort()',oddNumbers)
oddNumbers.sort()
console.log('Value of oddNumbers after sort()',oddNumbers)

const countries = ['US','PH','CAN','SG','PH']
// indexOf() finds the index of a given element where it is FIRST found
let indexCountry = countries.indexOf('PH')
console.log('Index of PH: ',indexCountry) // 1

// lastIndexOf() finds the index of a given element where it is last found
let lastIndexCountry = countries.lastIndexOf('PH')
console.log('Last Instance of PH: ',lastIndexCountry) // 4

console.log(countries)

// toString() converts an array into a single value that is separated by a comma
console.log(countries.toString())

const subTasksA = ['drink html','eat JS']
const subTasksB = ['inhale css','breathe sass']
const subTasksC = ['get git','be node']

// concat() joins 2 or more arrays
const subTasks = subTasksA.concat(subTasksB,subTasksC)
console.log(subTasks)

// join() converts an array into a single value, and is separated by a specified character
console.log(subTasks.join('-'))
console.log(subTasks.join('@'))
console.log(subTasks.join('x'))

const users = ['blue','alexis','bianca','nikko','adrian']

console.log(users[0]) //blue 
console.log(users[1]) //alexis

//syntax:
/*
	arrayName.forEach(function(eachElement){
		console.log(eachElement)
	})

*/

// forEach iterates each element inside an array
// we pass each element as the parameter of the function declared inside the forEach()

// we have users array
// we want to iterate the users array using the forEach function
// each element is stored in side the variable called user
// logged in the browser the value of each user / each element inside the users array

users.forEach(function(user) {
	// console.log('Name of Student: ',user)

	if(user == 'blue') {
		console.log('Name of Student: ',user)
	}
})

/* Mini Activity */

const numberList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,51,99,101,102]

numberList.forEach(function(number) {

	if(number % 2 == 0) {
		console.log(number,' is an even number')
	}
	else {
		console.log(number,' is an odd number')
	}
})

// length property returns the number of elements inside an array
console.log(numberList.length) //18

// map() iterates each element and returns a new array depending on the result of the function's operation
// map() is useful when we will manipulate/change elements inside our array
// map() allows us to not touch/manipulate the orignal array
const numberData = numberList.map(function(number) {
	return number * number
})

console.log(numberData)
console.log(numberList) // not manipulated

// multidimensional arrays

const chessBoard = [
	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4']
]

console.log(chessBoard) // logs the whole chessBoard array
console.log(chessBoard[0]) // logs the value in the index 0 of the chessboard array
console.log(chessBoard[0][1]) // specific element inside the array in index 0